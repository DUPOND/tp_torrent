/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xquery.controller;

import download.Download;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import xquery.config.Config;
import xquery.exceptions.SystemException;
import xquery.model.Model;

/**
 *
 * @author adrien
 */
public class Controller implements Runnable {

    private Path path;
    private Model model;
    private Config conf;

    public Controller(String pth, Model m, Config conf) {
        this.path = Paths.get(pth);
        this.model = m;
        this.conf = conf;
        System.out.println("Fichier :" + pth);
    }

    @Override
    public void run() {
        WatchService ws;
        try {
            ws = this.path.getFileSystem().newWatchService();
            //initialisation des actions au sein du dossier
            this.path.register(ws, StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);

            WatchKey watchKey;
            //surveillance du dossier
            while (!Thread.interrupted()) {
                watchKey = ws.take();

                for (WatchEvent<?> event : watchKey.pollEvents()) {
                    String fileName = event.context().toString();
                    if (StandardWatchEventKinds.ENTRY_CREATE.equals(event.kind())) {
                        System.out.println("new file create " + fileName);
                    } else if (StandardWatchEventKinds.ENTRY_MODIFY.equals(event.kind())) {
                        //en cas de modification du fichier rss.xml
                        if (fileName.equals("rss.xml")) {
                            this.exec();
                        }
                        System.out.println(fileName + " a été modifié");
                    } else if (StandardWatchEventKinds.ENTRY_DELETE.equals(event.kind())) {
                        System.out.println(fileName + " a été supprimé");
                    } else if (StandardWatchEventKinds.OVERFLOW.equals(event.kind())) {
                        System.out.println("Strange event");
                        continue;
                    }
                }
                watchKey.reset();
            }
        } catch (InterruptedException e) {

        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void exec() {
        String rss = conf.getProp(Config.FLX_RSS);
        String dest=conf.getProp(Config.DOWN_PATH);
        //requete pour les titres des series concernées
        String query = "for $title in /series/nom/text()\n"
                + "for $title2 in distinct-values(//title/text())\n"
                + "where contains($title2,$title)\n"
                + "return concat($title,';')";

        String pth = path.toString();
        try {
  
            String db = model.createDatabase();
            model.openDb(db);

            model.addXMLToDb(pth + "/" + rss);
            model.addXMLToDb(pth + "/series.xml");
            System.out.println(model.getDatabases());
            String titre = model.executeQuery(query);
            String[] tabTitre = titre.split(";");
            ArrayList<String> mapTitre = this.clean(tabTitre);
			 //array contenant tous les liens de telechargement
            ArrayList<String> linkList = new ArrayList<String>();
            /*
             parcours du tableau  pour creer des fichiers avec leur nom
             et leur contenu
             */
            for (String unTitre : mapTitre) {

                //suppression des espaces
                unTitre = unTitre.trim();
          
                //recuperation du contenu omdb
                String contenuOmbd = model.omdb(unTitre);
                
                System.out.println(contenuOmbd);
                //requête renvoyant la description
                String req_desc = "for $mdb in  /root/movie\n"
                        + "let $desc := distinct-values($mdb/@plot)\n"
                        + "let $title := $mdb/@title\n"
                        + "where contains($title,'" + unTitre + "')\n"
                        + "return\n"
                        + "<description>{(string($desc))}</description>";
                //requête renvoyant l'avis
                String req_avis = "for $mdb in  /root/movie\n"
                        + "let $desc := distinct-values($mdb/@plot)\n"
                        + "let $title := $mdb/@title\n"
                        + "let $avis  := $mdb/@imdbRating\n"
                        + "where contains($title,'" + unTitre + "')\n"
                        + "return\n"
                        + "<avis>{(string($avis))}</avis>";
                //requête renvoyant l'url de l'image
                String req_poster = "for $mdb in  /root/movie\n"
                        + "let $desc := distinct-values($mdb/@plot)\n"
                        + "let $title := $mdb/@title\n"
                        + "let $avis  := $mdb/@imdbRating\n"
                        + "let $poster  := $mdb/@poster\n"
                        + "where contains($title,'" + unTitre + "')\n"
                        + "return\n"
                        + "<poster>{(string($poster))}</poster>";
                

                model.createXml(contenuOmbd, pth);
                model.addXMLToDb(pth + "/omdb.xml");

                //requête renvoyant le nom de l'épisode et le lien 
                String q2 = "for $item in //item\n"
                        + "where contains($item/title/text(),'" + unTitre + "')\n"
                        + "return\n"
                        + "<episode>\n"
                        + "   <nom>{$item/title/text()}</nom>\n"
                        + "   <lien>{$item/enclosure/@url/string()}</lien>\n"
                        + "</episode>";

                String res = model.executeQuery(q2);
				
				//recuperation des liens a telecharger
                Pattern p = Pattern.compile(Pattern.quote("<lien>") + "(.*?)" + Pattern.quote("</lien>"));
                Matcher m = p.matcher(res);
                linkList=new ArrayList<String>();
                while (m.find()) {
                    linkList.add(m.group(1));
                }
		//execution des requêtes		
                String res_desc = model.executeQuery(req_desc);
                String res_avis = model.executeQuery(req_avis);
                String res_poster = model.executeQuery(req_poster);
                
                String res_omdb =res_desc+"\n"+res_avis+"\n"+res_poster+"\n";
                
                model.createXml(unTitre, res,res_omdb, pth);

	        
				
                model.removeXML(pth + "/omdb.xml");

            }
			
			//demarre le telechargement 
            Download d = new Download();
            d.autoDownload(linkList, dest);

        } catch (SystemException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //tableau ne contenant que des valeurs uniques
    private ArrayList<String> clean(String[] tab) {
        ArrayList<String> map = new ArrayList<>();
        int k = 0;
        for (int i = 0; i < tab.length; i++) {
            if (!map.contains(tab[i].trim())) {
                map.add(tab[i]);
            }
        }
        return map;
    }

}
