/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xquery.main;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.BasicConfigurator;
import xquery.config.Config;
import xquery.controller.Controller;
import xquery.exceptions.SystemException;
import xquery.model.Model;

/**
 *
 * @author adrien
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SystemException  {
       
       
        BasicConfigurator.configure();
        Config conf = new Config(Config.CONF_PTH);
        Model m = new Model(conf.getProp(Config.DATABASE), conf.getProp(Config.DB_PATH));
        String path = conf.getProp(Config.DB_PATH);
       
        //m.executeQuery(req);
        try {
   
            Controller ctr = new Controller(path,m,conf);
            ctr.exec();
            Thread th = new Thread(ctr);
            th.start();
            
         
            Thread.sleep(10000);
            Thread.interrupted();}
            
        catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }

}
