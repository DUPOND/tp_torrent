/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package download;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.Client.ClientState;
import com.turn.ttorrent.client.SharedTorrent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.apache.log4j.BasicConfigurator;

/**
 *
 * @author MichaelL
 */
public class Download {

    
    //telecharge tous les torrents 
    public void autoDownload(ArrayList<String> al,String dest){
        for (String s : al){
        String fileToDownload;
            try {
                fileToDownload = getTorrentFile(s,dest+"/"); //requete xquery en param
                downloadTorrent(fileToDownload,dest);//repertoire destination a externaliser dans le properties
            } catch (IOException ex) {
                Logger.getLogger(Download.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //recupere le fichier .torrent à partir du serveur
    public String getTorrentFile(String torrentLink,String repertoire) throws IOException {
        InputStream input = null;
        FileOutputStream writeFile = null;
        String fileName=null; 
        try {
            URL url = new URL(torrentLink);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                System.out.println("URL invalide");
                return fileName;
            }
            // le torrent est en format gzip
            input=new GZIPInputStream(connection.getInputStream());
            
            fileName = repertoire+url.getFile().substring(url.getFile().lastIndexOf('=') + 1)+".torrent"; //requete xquery pour obtenir le nom du fichier de destination
            writeFile = new FileOutputStream(fileName);
            byte[] buffer = new byte[1024];
            int read;

            while ((read = input.read(buffer)) > 0) {
                writeFile.write(buffer, 0, read);
            }
            writeFile.flush();
            
            
        } catch (IOException e) {
            System.out.println("Erreur lors du telechargement du fichier");
            e.printStackTrace();
        } finally {
            try {
                writeFile.close();
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileName;

    }

    //telecharge l'episode a partir du .torrent
    public void downloadTorrent(String torrentF, String folderDest) {
        File torrentFile = new File(torrentF);
        File folderD = new File(folderDest);

        try {
            SharedTorrent torrent = SharedTorrent.fromFile(torrentFile, folderD);
            Client cl = new Client(InetAddress.getLocalHost(), torrent);

            try {
                System.out.println("Début du téléchargement: " + torrent.getName());
                cl.download(); // commence le telechargement

                while (!ClientState.SEEDING.equals(cl.getState())) {
                    // verifie si il y'a une erreur
                    if (ClientState.ERROR.equals(cl.getState())) {
                        throw new Exception("Une erreur est survenue");
                    }

                    // affichage des statistiques
                    System.out.printf("%d %% - %d OCTETS TELECHARGES\n", (int) torrent.getCompletion(), torrent.getDownloaded());

                    TimeUnit.SECONDS.sleep(1);
                }

                System.out.println("TELECHARGEMENT TERMINE");
            } catch (Exception e) {
                System.err.println("Une erreur est survenue");
                e.printStackTrace(System.err);
            } finally {
                System.out.println("ARRET DU CLIENT");
                cl.stop();
            }
        } catch (Exception e) {
            System.err.println("une erreur est survenue");
            e.printStackTrace(System.err);
        }
    }
}
