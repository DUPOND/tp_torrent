<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output
        method="html"
        encoding="UTF-8"
        indent="yes"/>

	<xsl:template match="/">
		<html>
			<head>
				<link type="text/css" rel="stylesheet" href="serie.css" />
			</head>
			<body>
				<section>

					<div id="titre">
						<h1>
							<xsl:value-of select="/serie/nom"/>
						</h1>
					</div>

					<div style="display: inline-block">
						<!-- Ajout de l'image de la série -->
						<div style="float:left; width:49.5%; heigth: 100px">
							<xsl:element name="img">
  								<xsl:attribute name="src">
  									<xsl:value-of select="/serie/poster"/>
  								</xsl:attribute>
							</xsl:element>
						</div>

						<div style="float:rigth; max-width:49.5">
							<div>
								<p>
									<span class="underline">Description :</span>
									<xsl:value-of select="/serie/description"/>
								</p>
							</div>
							<div>
								<p>
									<span class="underline">Note :</span>
									<xsl:value-of select="/serie/avis"/>/10
								</p>
							</div>
						</div>
					</div>
					<!-- Permet d'afficher l'ensemble des épisodes ou l'épisode-->
					<xsl:if test="count(/serie/episodes/episode)>1">
						<span class="underline">Épisodes :</span>
					</xsl:if>
					<xsl:if test="count(/serie/episodes/episode)=1">
						<span class="underline">Épisode :</span>
					</xsl:if>
					<xsl:for-each select="/serie/episodes/episode">
						<div id="episode">
							<div id="nom">
								<p>
									<xsl:value-of select="nom"/>
								</p>
							</div>
							<div id="lien">
								<xsl:element name="a">
										<xsl:attribute name="href">
											<xsl:value-of select="lien"/>
										</xsl:attribute>
										<xsl:value-of select="lien"/>
								</xsl:element>
							</div>
						</div>
					</xsl:for-each>
				</section>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>